# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.0] - 2023-01-19

### Added

- New CSV export format
- New Prometheus format
- Include date of last pipeline run
- Limit crawling on the date of the last pipeline's date (`--since`, `--until`)

## [0.2.0] - 2023-01-17

### Added

- Filter on a list of groups
- Display the template file used

### Fixed

- Deal with some syntax errors in `.gitlab-ci.yml` parsed.

## [0.1.2] - 2023-01-16

### Fixed

- Use default branch
- Deal with possible redefinition of CI's path
- Support mix of include types (`local:`, `remote:`, `project:`)
- Enforce logic on CLI parameters

## [0.1.1] - 2023-01-13

### Fixed

- Fix the dead loop on projects

## [0.1.0] - 2023-01-13

Initial release.

### Added

- Extract templates from all accessible projects
