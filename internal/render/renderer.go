package render

import (
	"tbc/template-crawler/internal/model"
	"time"

	"github.com/xanzy/go-gitlab"
)

type Renderer interface {
	Start()
	Add(proj *gitlab.Project, incl *model.Include, date *time.Time)
	Stop()
}
