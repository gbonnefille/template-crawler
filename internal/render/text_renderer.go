package render

import (
	"fmt"
	"tbc/template-crawler/internal/model"
	"time"

	"github.com/xanzy/go-gitlab"
)

type TextRenderer struct {
	DateFormat string
}

func (r TextRenderer) Start() {
	// Nothing
}

func (r TextRenderer) Add(proj *gitlab.Project, incl *model.Include, date *time.Time) {
	fmt.Printf("The project %s uses the template %s from the project %s in version %s", proj.PathWithNamespace, incl.File, incl.Project, incl.Ref)
	if date != nil {
		fmt.Printf(" on date %s", date.Local().Format(r.DateFormat))
	}
	fmt.Println()
}

func (r TextRenderer) Stop() {
	// Nothing
}
