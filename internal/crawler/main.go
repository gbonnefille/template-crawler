/*
 * Copyright (C) 2022 Guilhem Bonnefille <guilhem.bonnefille@csgroup.eu>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 *
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package crawler

import (
	"encoding/base64"
	"tbc/template-crawler/internal/logging"
	"tbc/template-crawler/internal/model"
	"tbc/template-crawler/internal/render"
	"time"

	"github.com/xanzy/go-gitlab"
)

func getLastPipelineDate(git *gitlab.Client, proj *gitlab.Project, branch string) *time.Time {
	opt := gitlab.GetLatestPipelineOptions{
		Ref: gitlab.String(branch),
	}
	pipeline, _, err := git.Pipelines.GetLatestPipeline(proj.ID, &opt)
	if err != nil {
		logging.Debug().Printf("Failed to fetch last pipeline for project %s: %v", proj.PathWithNamespace, err)
		return nil
	} else {
		return pipeline.CreatedAt
	}
}

func AnalyseProject(r render.Renderer, dateFilter DateFilter, git *gitlab.Client, proj *gitlab.Project) {
	branch := proj.DefaultBranch
	logging.Debug().Printf("Default branch of project %s: %s", proj.Name, branch)
	gf := gitlab.GetFileOptions{
		Ref: gitlab.String(branch),
	}
	gitlabcipath := proj.CIConfigPath
	if gitlabcipath == "" {
		gitlabcipath = ".gitlab-ci.yml"
	}
	logging.Debug().Printf("CI file of project %s: %s", proj.Name, gitlabcipath)
	file, _, err := git.RepositoryFiles.GetFile(proj.ID, gitlabcipath, &gf)
	if err != nil {
		logging.Debug().Printf("File %s not found in project %s: %s", gitlabcipath, proj.Name, err)
	} else {
		content, err := base64.StdEncoding.DecodeString(file.Content)
		if err != nil {
			logging.Error().Fatal(err)
		}
		data := model.ParseGitlabCiYaml(content)
		// Fetch pipeline date
		date := getLastPipelineDate(git, proj, branch)
		if dateFilter.Match(date) {
			// Render information
			for _, incl := range data.GetProjectInclude() {
				// Reasign the iteration variable in order to have a long running address
				incl := incl
				r.Add(proj, &incl, date)
			}
		}
	}
}

func CrawlGroups(renderer render.Renderer, dateFilter DateFilter, git *gitlab.Client, groups []string) {
	for _, groupId := range groups {
		logging.Info().Printf("Looking for group %s", groupId)
		// Check group
		group, _, err := git.Groups.GetGroup(groupId, &gitlab.GetGroupOptions{})
		if err != nil {
			logging.Error().Printf("Failed to retrieve group %s: %v", groupId, err)
			continue
		}

		// Retrieve projects of group
		opt := &gitlab.ListGroupProjectsOptions{
			ListOptions: gitlab.ListOptions{
				PerPage: 20,
				Page:    1,
			},
			IncludeSubGroups: gitlab.Bool(true),
		}
		for {
			projects, resp, err := git.Groups.ListGroupProjects(group.ID, opt)
			if err != nil {
				logging.Error().Fatal(err)
			}

			for _, project := range projects {
				logging.Info().Printf("Processing project %s", project.Name)
				AnalyseProject(renderer, dateFilter, git, project)
			}

			// Exit the loop when we've seen all pages.
			if resp.NextPage == 0 {
				break
			}

			// Update the page number to get the next page.
			opt.Page = resp.NextPage
		}
	}
}

func CrawlProjects(renderer render.Renderer, dateFilter DateFilter, git *gitlab.Client) {
	opt := &gitlab.ListProjectsOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 20,
			Page:    1,
		},
	}
	for {
		projects, resp, err := git.Projects.ListProjects(opt)
		if err != nil {
			logging.Error().Fatal(err)
		}

		for _, project := range projects {
			logging.Info().Printf("Processing project %s", project.Name)
			AnalyseProject(renderer, dateFilter, git, project)
		}

		// Exit the loop when we've seen all pages.
		if resp.NextPage == 0 {
			break
		}

		// Update the page number to get the next page.
		opt.Page = resp.NextPage
	}
}

func Crawl(renderer render.Renderer, dateFilter DateFilter, git *gitlab.Client, groups []string) {
	renderer.Start()
	if len(groups) > 0 {
		CrawlGroups(renderer, dateFilter, git, groups)
	} else {
		CrawlProjects(renderer, dateFilter, git)
	}
	renderer.Stop()
}
