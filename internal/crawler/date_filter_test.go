package crawler

import (
	"fmt"
	"testing"
	"time"
)

func TestFilter(t *testing.T) {
	until := time.Now()
	since := until.Add(-time.Minute)
	f := DateFilter{Until: &until, Since: &since}
	fmt.Println(f)

	// Edges
	if !f.Match(&since) {
		t.Error("Expecting since inside")
	}
	if !f.Match(&until) {
		t.Error("Expecting until inside")
	}

	// Before
	before := since.Add(-time.Minute)
	if f.Match(&before) {
		t.Errorf("Before date should not match: %s <> %s", f, before)
	}

	// After
	after := until.Add(time.Minute)
	if f.Match(&after) {
		t.Errorf("After date should not match: %s <> %s", f, after)
	}
}

func TestParseDurationBasedFilter(t *testing.T) {
	f := ParseDateFilter("", "-10h", "10m")
	fmt.Println(f)
}

func TestParseInstantBasedFilter(t *testing.T) {
	f := ParseDateFilter("2006-01-02T15:04", "2001-03-06T01:02", "2002-03-06T01:02")
	fmt.Println(f)
}
