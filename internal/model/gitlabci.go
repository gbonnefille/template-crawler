package model

import (
	"regexp"
	"tbc/template-crawler/internal/logging"

	"gopkg.in/yaml.v3"
)

type Include struct {
	Project string `yaml:"project"`
	Ref     string `yaml:"ref"`
	File    string `yaml:"file"`
}
type GitlabCI struct {
	Includes []interface{} `yaml:"include"`
}

func ParseGitlabCiYaml(content []byte) GitlabCI {
	var data GitlabCI
	if err := yaml.Unmarshal(content, &data); err != nil {
		// Do not fail due to remote content
		logging.Warn().Print(err)
	}
	logging.Debug().Print("Read", data)
	return data
}

func (p *GitlabCI) GetProjectInclude() []Include {
	res := make([]Include, 0)
	for _, raw := range p.Includes {
		st, ok := raw.(map[string]interface{})
		if ok {
			if _, ok := st["project"]; ok {
				project, ok := st["project"].(string)
				if !ok {
					logging.Debug().Printf("Failed to convert project: value as string: %v", st["project"])
					continue
				}
				file, ok := st["file"].(string)
				if !ok {
					logging.Debug().Printf("Failed to convert file: value as string: %v", st["file"])
					continue
				}
				ref, ok := st["ref"].(string)
				if !ok {
					// ref: is optional
					// https://docs.gitlab.com/ee/ci/yaml/#includeproject
					ref = "HEAD"
				}
				inc := Include{
					Project: project,
					Ref:     ref,
					File:    file,
				}
				res = append(res, inc)
			} else if _, ok := st["component"]; ok {
				comp, ok := st["component"].(string)
				if !ok {
					logging.Debug().Printf("Failed to convert component: value as string: %v", st["component"])
					continue
				}
				inc := extractComponentParts(comp)
				if inc == nil {
					logging.Debug().Printf("Failed to extract component's part from '%s'", comp)
					continue
				}
				res = append(res, *inc)
			}
		}
	}
	return res
}

var component *regexp.Regexp

func init() {
	// <fully-qualified-domain-name>/<project-path>/<component-name>@<specific-version>
	component = regexp.MustCompile(`(.*)/([^/]*)@(.*)`)
}

func extractComponentParts(st string) *Include {
	parts := component.FindStringSubmatch(st)
	if len(parts) == 4 {
		comp := Include{
			Project: parts[1],
			File:    parts[2],
			Ref:     parts[3],
		}
		return &comp
	}
	return nil
}
