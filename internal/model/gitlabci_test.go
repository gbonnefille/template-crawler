package model

import (
	"testing"
)

func TestExtractComponentParts(t *testing.T) {
	var tests = []struct {
		s    string
		want *Include
	}{
		{
			s:    "incorrect",
			want: nil,
		},
		{
			s: "gitlab.com/to-be-continuous/docker/gitlab-ci-docker@5.8.1",
			want: &Include{
				Project: "gitlab.com/to-be-continuous/docker",
				File:    "gitlab-ci-docker",
				Ref:     "5.8.1",
			},
		},
	}

	for _, tt := range tests {
		testname := tt.s
		t.Run(testname, func(t *testing.T) {
			ans := extractComponentParts(tt.s)
			if ans != nil && tt.want != nil && *ans != *tt.want {
				t.Errorf("got %v, want %v", ans, tt.want)
			}
		})
	}
}

func TestProjectInclude(t *testing.T) {
	data := `
include:
  - project: 'to-be-continuous/golang'
    ref: '4.1.0'
    file: '/templates/gitlab-ci-golang.yml'
`
	gc := ParseGitlabCiYaml([]byte(data))
	if len(gc.GetProjectInclude()) != 1 {
		t.Errorf("Expecting 1 got %d", len(gc.GetProjectInclude()))
	}
}

// Single include
func TestLocalInclude(t *testing.T) {
	data := `
include: '/gitlab-ci-golang.yml'
`
	gc := ParseGitlabCiYaml([]byte(data))
	if len(gc.GetProjectInclude()) != 0 {
		t.Errorf("Expecting 0 got %d", len(gc.GetProjectInclude()))
	}
}

// Multiple includes not project
func TestUnsupportedIncludes(t *testing.T) {
	data := `
include:
- remote: 'https://gitlab.com/awesome-project/raw/main/.before-script-template.yml'
- local: '/templates/.after-script-template.yml'
- template: Auto-DevOps.gitlab-ci.yml
`
	gc := ParseGitlabCiYaml([]byte(data))
	if len(gc.GetProjectInclude()) != 0 {
		t.Errorf("Expecting 0 got %d: %v", len(gc.GetProjectInclude()), gc.Includes)
	}
}
