/*
 * Copyright (C) 2022 Guilhem Bonnefille <guilhem.bonnefille@csgroup.eu>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 *
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package cmd

import (
	"tbc/template-crawler/internal/crawler"
	"tbc/template-crawler/internal/logging"
	"tbc/template-crawler/internal/render"

	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
)

var (
	// Used for flags.
	verbose bool
	debug   bool

	version string = "unset"

	user  string
	pass  string
	token string

	base string

	groups []string

	format string

	dateFormat string

	since string
	until string

	rootCmd = &cobra.Command{
		Use:     "template-crawler",
		Short:   "An utility to retrieve the Gitlab CI templates used by projects.",
		Version: version,
		PersistentPreRun: func(cmd *cobra.Command, args []string) {
			logging.Configure(debug, verbose)
		},
		Run: func(cmd *cobra.Command, args []string) {
			crawl()
		},
	}
)

// Execute executes the root command.
func Execute() error {
	return rootCmd.Execute()
}

func init() {
	rootCmd.PersistentFlags().BoolVar(&debug, "debug", false, "activate debug messages")
	rootCmd.PersistentFlags().BoolVar(&verbose, "verbose", false, "make the output more verbose")

	rootCmd.Flags().StringVarP(&user, "username", "u", "", "Username")
	rootCmd.Flags().StringVarP(&pass, "password", "p", "", "Password")
	rootCmd.MarkFlagsRequiredTogether("username", "password")

	rootCmd.Flags().StringVarP(&token, "token", "t", "", "Token")
	rootCmd.MarkFlagsMutuallyExclusive("username", "token")

	rootCmd.Flags().StringVarP(&base, "base", "b", "https://gitlab.com", "Base URL")

	rootCmd.Flags().StringArrayVarP(&groups, "group", "g", []string{}, "Groups names")

	rootCmd.Flags().StringVarP(&format, "format", "f", "text", "Format (text or csv or prom)")

	rootCmd.Flags().StringVarP(&dateFormat, "date-format", "", "2006-01-02T15:04:05-07:00", "Format of date (golang date's format specification, see https://pkg.go.dev/time#example-Time.Format)")

	rootCmd.Flags().StringVarP(&since, "since", "", "", "Lower date limit to retain data based on last pipeline date: can be a duration from now (10h, -1m) or instant time following --date-format")
	rootCmd.Flags().StringVarP(&until, "until", "", "", "Upper date limit to retain data based on last pipeline date: can be a duration from now (10h, -1m) or instant time following --date-format")
}

func crawl() {
	var git *gitlab.Client = nil
	var err error

	if user != "" {
		git, err = gitlab.NewBasicAuthClient(
			user,
			pass,
			gitlab.WithBaseURL(base),
		)
		if err != nil {
			logging.Error().Fatal(err)
		}
	}

	if token != "" {
		git, err = gitlab.NewClient(
			token,
			gitlab.WithBaseURL(base),
		)
		if err != nil {
			logging.Error().Fatal(err)
		}
	}

	if git == nil {
		logging.Error().Fatal("Username or Token is mandatory")
	}

	var renderer render.Renderer
	switch format {
	case "text":
		renderer = render.TextRenderer{
			DateFormat: dateFormat,
		}
	case "csv":
		renderer = render.NewCsvRenderer(dateFormat)
	case "prom":
		renderer = render.NewPrometheusRenderer()
	default:
		logging.Error().Fatalf("Unknown format '%s'", format)
	}

	dateFilter := crawler.ParseDateFilter(format, since, until)

	crawler.Crawl(renderer, dateFilter, git, groups)
}
